class Display:
    def __init__(self, nprobes):
        self.nprobes = nprobes

    def show_field(self, idx, entry, key, subkey, legend, units):

        if None == entry:
            return

        if not key in entry:
            return

        if None != subkey:
            if not subkey in entry[key]:
                return

            value = entry[key][subkey]

        else:
            value = entry[key]

        if isinstance(value, int):
            print "%d. %-14s : %8d %s" % (idx, legend, value, units)
        elif isinstance(value, float):
            print "%d. %-14s : %8.2f %s" % (idx, legend, value, units)
        else:
            print "%d. %-14s : %8s %s" % (idx, legend, value, units)


    def show(self, entry):
        print chr(27) + "[2J"

        self.show_field(0, entry, 'samples', None, "Samples", "/sec")
        self.show_field(0, entry, 'voltage', None, "Voltage RMS", "V")
        self.show_field(0, entry, 'frequency', None, "Frequency", "Hz")
        self.show_field(0, entry, 'dcoffset', None, "DC Offset", "")
        self.show_field(0, entry, 'temperature', None, "Temperature", "C")

        print

        for probe in range(1, self.nprobes + 2):
            self.show_field(probe, entry, 'current_' + str(probe),
                            'power_real', "Power real", "W")

        print

        for probe in range(1, self.nprobes + 2):
            self.show_field(probe, entry, 'current_' + str(probe),
                            'power_apparent', "Power app", "W")

        print

        for probe in range(1, self.nprobes + 2):
            self.show_field(probe, entry, 'current_' + str(probe),
                            'current', "Current", "A")

        print

        for probe in range(1, self.nprobes + 2):
            self.show_field(probe, entry, 'current_' + str(probe),
                            'current_max', "Current Max", "A")

        print

        for probe in range(1, self.nprobes + 2):
            self.show_field(probe, entry, 'current_' + str(probe),
                            'power_factor', "Power factor", "")

        print

        for probe in range(1, self.nprobes + 1):
            self.show_field(probe, entry, 'current_' + str(probe),
                            'gain', "Gain", "")

