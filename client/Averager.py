class Averager:
    count = 0
    items = []

    def __init__(self, nprobes, count):
        self.count   = count
        self.nprobes = nprobes

    def average(self, key, parent_key):
        if None == parent_key:
            values = [ item[key] for item in self.items if key in item ]
        else:
            values = [ item[parent_key][key] for item in self.items if parent_key in item and key in item[parent_key] ]

        if len(values) > 0:
            return round(sum(values) / len(values), 2)
        else:
            return None

    def average_items(self):
        average = {}

        average['dcoffset']    = self.average('dcoffset',    None)
        average['samples']     = self.average('samples',     None)
        average['timems']      = self.average('timems',      None)
        average['temperature'] = self.average('temperature', None)
        average['voltage']     = self.average('voltage',     None)
        average['frequency']   = self.average('frequency',   None)
        for probe in range(1, self.nprobes + 2):
            current = "current_" + str(probe)

            average[current] = {}
            average[current]['power_real']     = self.average('power_real',     current)
            average[current]['power_apparent'] = self.average('power_apparent', current)
            average[current]['current']        = self.average('current',        current)

        for probe in range(1, self.nprobes + 1):
            current = "current_" + str(probe)

            average[current]['current_max']    = self.average('current_max',    current)
            average[current]['power_factor']   = self.average('power_factor',   current)

        return average

    def append(self, entry):
        retval = None

        if {} == entry:
            return None

        self.items.append(entry)

        if len(self.items) >= self.count:
            retval = self.average_items()
            self.items = []

        return retval

