EESchema Schematic File Version 2
LIBS:lvm324
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ARDUINO_NANO
LIBS:emon-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "31 mar 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 9900 6200 0    60   ~ 0
Vmid
Text Label 9900 5800 0    60   ~ 0
Vh
Text Label 8600 6200 0    60   ~ 0
Va
Text Label 8600 5800 0    60   ~ 0
Vb
$Comp
L TRANSFO T1
U 1 1 528CF34F
P 9250 6000
F 0 "T1" H 9250 6250 70  0000 C CNN
F 1 "TRANSFO" H 9250 5700 70  0000 C CNN
F 2 "Transformers_CHK:Trafo_CHK-EI30-2VA_1xSec" H 9250 6000 60  0001 C CNN
F 3 "" H 9250 6000 60  0001 C CNN
	1    9250 6000
	1    0    0    -1  
$EndComp
Text Label 6650 3900 0    60   ~ 0
D4
Text Label 6650 4200 0    60   ~ 0
D7
Text Label 10000 4000 0    60   ~ 0
CLAMP3out
$Comp
L GND #PWR01
U 1 1 5283BC92
P 10650 3950
F 0 "#PWR01" H 10650 3950 30  0001 C CNN
F 1 "GND" H 10650 3880 30  0001 C CNN
F 2 "" H 10650 3950 60  0001 C CNN
F 3 "" H 10650 3950 60  0001 C CNN
	1    10650 3950
	1    0    0    -1  
$EndComp
Text Label 10000 4200 0    60   ~ 0
CLAMP2out
Text Label 10000 4400 0    60   ~ 0
Vmid
$Comp
L CONN_2 P4
U 1 1 5283BA23
P 4500 5850
F 0 "P4" V 4450 5850 40  0000 C CNN
F 1 "CUR3" V 4550 5850 40  0000 C CNN
F 2 "JACK-3.5M" H 4500 5850 60  0001 C CNN
F 3 "" H 4500 5850 60  0001 C CNN
	1    4500 5850
	-1   0    0    1   
$EndComp
$Comp
L CONN_2 P3
U 1 1 5283B962
P 4500 6250
F 0 "P3" V 4450 6250 40  0000 C CNN
F 1 "CUR2" V 4550 6250 40  0000 C CNN
F 2 "JACK-3.5M" H 4500 6250 60  0001 C CNN
F 3 "" H 4500 6250 60  0001 C CNN
	1    4500 6250
	-1   0    0    1   
$EndComp
NoConn ~ 9700 4700
NoConn ~ 9700 4600
NoConn ~ 6900 4700
NoConn ~ 6900 4600
NoConn ~ 6900 4500
NoConn ~ 6900 4100
NoConn ~ 6900 4000
NoConn ~ 6900 3800
Text Label 5150 5300 2    60   ~ 0
Vb
Text Label 5150 5500 2    60   ~ 0
Va
Text Label 5350 6550 2    60   ~ 0
CLAMP1in
$Comp
L CONN_2 P1
U 1 1 5271FE45
P 4500 5400
F 0 "P1" V 4450 5400 40  0000 C CNN
F 1 "VOLT" V 4550 5400 40  0000 C CNN
F 2 "Connect:bornier2" H 4500 5400 60  0001 C CNN
F 3 "" H 4500 5400 60  0001 C CNN
	1    4500 5400
	-1   0    0    1   
$EndComp
$Comp
L CONN_2 P2
U 1 1 5271FE3D
P 4500 6650
F 0 "P2" V 4450 6650 40  0000 C CNN
F 1 "CUR1" V 4550 6650 40  0000 C CNN
F 2 "JACK-3.5M" H 4500 6650 60  0001 C CNN
F 3 "" H 4500 6650 60  0001 C CNN
	1    4500 6650
	-1   0    0    1   
$EndComp
Text Label 6100 6200 0    60   ~ 0
Vout
Text Label 6500 5400 0    60   ~ 0
Vh
Text Label 10000 4100 0    60   ~ 0
CLAMP1out
Text Label 6650 4300 0    60   ~ 0
D8
Text Label 9900 5050 0    60   ~ 0
D9
$Comp
L GND #PWR02
U 1 1 5271450A
P 9900 5500
F 0 "#PWR02" H 9900 5500 30  0001 C CNN
F 1 "GND" H 9900 5430 30  0001 C CNN
F 2 "" H 9900 5500 60  0001 C CNN
F 3 "" H 9900 5500 60  0001 C CNN
	1    9900 5500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 52714488
P 9900 3450
F 0 "#PWR03" H 9900 3450 30  0001 C CNN
F 1 "GND" H 9900 3380 30  0001 C CNN
F 2 "" H 9900 3450 60  0001 C CNN
F 3 "" H 9900 3450 60  0001 C CNN
	1    9900 3450
	1    0    0    -1  
$EndComp
Text Label 10000 4300 0    60   ~ 0
Vout
$Comp
L R R2
U 1 1 5270AFE5
P 6500 5850
F 0 "R2" V 6580 5850 50  0000 C CNN
F 1 "100K" V 6500 5850 50  0000 C CNN
F 2 "Resistors_SMD:R_1206" H 6500 5850 60  0001 C CNN
F 3 "" H 6500 5850 60  0001 C CNN
	1    6500 5850
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 5270AFD9
P 6500 6550
F 0 "R1" V 6580 6550 50  0000 C CNN
F 1 "15K" V 6500 6550 50  0000 C CNN
F 2 "Resistors_SMD:R_1206" H 6500 6550 60  0001 C CNN
F 3 "" H 6500 6550 60  0001 C CNN
	1    6500 6550
	1    0    0    -1  
$EndComp
$Comp
L SPEAKER SP1
U 1 1 5270AC4B
P 10550 5250
F 0 "SP1" H 10450 5500 70  0000 C CNN
F 1 "SPEAKER" H 10450 5000 70  0000 C CNN
F 2 "Divers:BUZZER" H 10550 5250 60  0001 C CNN
F 3 "" H 10550 5250 60  0001 C CNN
	1    10550 5250
	1    0    0    -1  
$EndComp
$Comp
L ARDUINO_NANO_RIGHT_RAIL U2
U 1 1 5270AB33
P 8950 4000
F 0 "U2" H 8950 3900 50  0000 C CNN
F 1 "ARDUINO_NANO_RIGHT_RAIL" V 8750 4100 50  0000 C CNN
F 2 "ARDUINO_NANO_RIGHT_RAIL" H 8950 4000 50  0001 C CNN
F 3 "DOCUMENTATION" H 8950 4000 50  0001 C CNN
	1    8950 4000
	1    0    0    -1  
$EndComp
$Comp
L ARDUINO_NANO_LEFT_RAIL U1
U 1 1 5270AB1D
P 7650 4000
F 0 "U1" H 7850 3900 50  0000 C CNN
F 1 "ARDUINO_NANO_LEFT_RAIL" V 7650 4100 50  0000 C CNN
F 2 "SIL-15" H 7650 4000 50  0001 C CNN
F 3 "DOCUMENTATION" H 7650 4000 50  0001 C CNN
	1    7650 4000
	1    0    0    -1  
$EndComp
Text Label 10000 4500 0    60   ~ 0
3.3V
$Comp
L +5V #PWR04
U 1 1 53995EBD
P 5850 1450
F 0 "#PWR04" H 5850 1540 20  0001 C CNN
F 1 "+5V" H 5850 1540 30  0000 C CNN
F 2 "" H 5850 1450 60  0000 C CNN
F 3 "" H 5850 1450 60  0000 C CNN
	1    5850 1450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 53995FB5
P 7850 2550
F 0 "#PWR05" H 7850 2550 30  0001 C CNN
F 1 "GND" H 7850 2480 30  0001 C CNN
F 2 "" H 7850 2550 60  0000 C CNN
F 3 "" H 7850 2550 60  0000 C CNN
	1    7850 2550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 53995FC2
P 6400 2150
F 0 "#PWR06" H 6400 2150 30  0001 C CNN
F 1 "GND" H 6400 2080 30  0001 C CNN
F 2 "" H 6400 2150 60  0000 C CNN
F 3 "" H 6400 2150 60  0000 C CNN
	1    6400 2150
	1    0    0    -1  
$EndComp
Text Label 7050 1650 0    60   ~ 0
3.3V
$Comp
L C C4
U 1 1 53996423
P 6100 2050
F 0 "C4" V 6061 2148 40  0000 L CNN
F 1 "0.47uF" V 6198 1971 40  0000 L TNN
F 2 "Capacitors_SMD:C_1206" H 6138 1900 30  0000 C CNN
F 3 "~" H 6100 2050 60  0000 C CNN
	1    6100 2050
	0    1    1    0   
$EndComp
$Comp
L R R3
U 1 1 53996807
P 7850 1450
F 0 "R3" V 7930 1450 50  0000 C CNN
F 1 "12K" V 7850 1450 50  0000 C CNN
F 2 "Resistors_SMD:R_1206" H 7850 1450 60  0001 C CNN
F 3 "" H 7850 1450 60  0001 C CNN
	1    7850 1450
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 5399680D
P 7850 2150
F 0 "R4" V 7930 2150 50  0000 C CNN
F 1 "12K" V 7850 2150 50  0000 C CNN
F 2 "Resistors_SMD:R_1206" H 7850 2150 60  0001 C CNN
F 3 "" H 7850 2150 60  0001 C CNN
	1    7850 2150
	1    0    0    -1  
$EndComp
Text Label 7850 1000 0    60   ~ 0
3.3V
$Comp
L LVM324 U3
U 1 1 539C83A3
P 2600 5350
F 0 "U3" H 2600 5750 60  0000 C CNN
F 1 "LMV324" V 2600 5350 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 2600 5350 60  0000 C CNN
F 3 "~" H 2600 5350 60  0000 C CNN
	1    2600 5350
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR07
U 1 1 539C8629
P 1000 5250
F 0 "#PWR07" H 1000 5340 20  0001 C CNN
F 1 "+5V" H 1000 5340 30  0000 C CNN
F 2 "" H 1000 5250 60  0001 C CNN
F 3 "" H 1000 5250 60  0001 C CNN
	1    1000 5250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR08
U 1 1 539C862F
P 4100 5400
F 0 "#PWR08" H 4100 5400 30  0001 C CNN
F 1 "GND" H 4100 5330 30  0001 C CNN
F 2 "" H 4100 5400 60  0001 C CNN
F 3 "" H 4100 5400 60  0001 C CNN
	1    4100 5400
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR09
U 1 1 539C8737
P 3800 6300
F 0 "#PWR09" H 3800 6390 20  0001 C CNN
F 1 "+5V" H 3800 6390 30  0000 C CNN
F 2 "" H 3800 6300 60  0001 C CNN
F 3 "" H 3800 6300 60  0001 C CNN
	1    3800 6300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 539C87E6
P 1100 7000
F 0 "#PWR010" H 1100 7000 30  0001 C CNN
F 1 "GND" H 1100 6930 30  0001 C CNN
F 2 "" H 1100 7000 60  0001 C CNN
F 3 "" H 1100 7000 60  0001 C CNN
	1    1100 7000
	1    0    0    -1  
$EndComp
$Comp
L 74HC4066 U4
U 1 1 539C85E1
P 2200 6650
F 0 "U4" H 2200 7050 60  0000 C CNN
F 1 "74HC4066" V 2200 6650 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 2200 6650 60  0000 C CNN
F 3 "~" H 2200 6650 60  0000 C CNN
	1    2200 6650
	1    0    0    -1  
$EndComp
$Comp
L R R11
U 1 1 539C8B84
P 3650 1050
F 0 "R11" V 3730 1050 50  0000 C CNN
F 1 "470K" V 3650 1050 50  0000 C CNN
F 2 "Resistors_SMD:R_1206" H 3650 1050 60  0001 C CNN
F 3 "" H 3650 1050 60  0001 C CNN
	1    3650 1050
	0    -1   -1   0   
$EndComp
Text Label 1250 5150 0    60   ~ 0
OPAMP1in-
Text Label 1250 5050 0    60   ~ 0
OPAMP1out
Text Label 1250 5250 0    60   ~ 0
OPAMP1in+
Text Label 1250 5450 0    60   ~ 0
OPAMP2in+
Text Label 1250 5550 0    60   ~ 0
OPAMP2in-
Text Label 1250 5650 0    60   ~ 0
OPAMP2out
Text Label 3950 5450 2    60   ~ 0
OPAMP3in+
Text Label 3950 5550 2    60   ~ 0
OPAMP3in-
Text Label 3950 5650 2    60   ~ 0
OPAMP3out
$Comp
L OPAMP U8
U 1 1 539C92DC
P 3700 1600
F 0 "U8" H 3850 1900 70  0000 C CNN
F 1 "OPAMP1" H 3850 1800 70  0000 C CNN
F 2 "" H 3700 1600 60  0001 C CNN
F 3 "" H 3700 1600 60  0000 C CNN
	1    3700 1600
	1    0    0    1   
$EndComp
$Comp
L SWITCH U14
U 1 1 539C947E
P 2650 1500
F 0 "U14" H 2700 1300 30  0000 L CNN
F 1 "SWITCH1" H 2650 1350 30  0000 L CNN
F 2 "" H 2650 1500 60  0001 C CNN
F 3 "" H 2650 1500 60  0000 C CNN
	1    2650 1500
	1    0    0    -1  
$EndComp
$Comp
L R R8
U 1 1 539C948B
P 2750 1050
F 0 "R8" V 2830 1050 50  0000 C CNN
F 1 "680K" V 2750 1050 50  0000 C CNN
F 2 "Resistors_SMD:R_1206" H 2750 1050 60  0001 C CNN
F 3 "" H 2750 1050 60  0001 C CNN
	1    2750 1050
	0    -1   -1   0   
$EndComp
$Comp
L R R5
U 1 1 539C9617
P 1950 1050
F 0 "R5" V 2030 1050 50  0000 C CNN
F 1 "100K" V 1950 1050 50  0000 C CNN
F 2 "Resistors_SMD:R_1206" H 1950 1050 60  0001 C CNN
F 3 "" H 1950 1050 60  0001 C CNN
	1    1950 1050
	0    -1   -1   0   
$EndComp
$Comp
L CAPAPOL C1
U 1 1 539C9AA9
P 1350 1050
F 0 "C1" V 1400 1150 40  0000 L CNN
F 1 "10uF" V 1300 1150 40  0000 L CNN
F 2 "SMD_Packages:SMD-1206_Pol" H 1450 900 30  0000 C CNN
F 3 "~" H 1350 1050 300 0000 C CNN
	1    1350 1050
	0    1    1    0   
$EndComp
Text Label 4200 1600 0    60   ~ 0
OPAMP1out
Text Label 3550 1950 2    60   ~ 0
OPAMP1in+
Text Label 2700 1950 0    60   ~ 0
Vmid
Text Label 3700 1300 2    60   ~ 0
OPAMP1in-
Text Label 5350 6150 2    60   ~ 0
CLAMP2in
Text Label 5350 5750 2    60   ~ 0
CLAMP3in
$Comp
L GND #PWR011
U 1 1 539CA701
P 5500 6850
F 0 "#PWR011" H 5500 6850 30  0001 C CNN
F 1 "GND" H 5500 6780 30  0001 C CNN
F 2 "" H 5500 6850 60  0001 C CNN
F 3 "" H 5500 6850 60  0001 C CNN
	1    5500 6850
	1    0    0    -1  
$EndComp
Text Label 750  1050 0    60   ~ 0
CLAMP1in
Text Label 1050 6450 0    60   ~ 0
SWITCH1a
Text Label 1050 6350 0    60   ~ 0
SWITCH1b
Text Label 1050 6550 0    60   ~ 0
SWITCH2b
Text Label 1050 6650 0    60   ~ 0
SWITCH2a
Text Label 1050 6750 0    60   ~ 0
SWITCH2e
Text Label 1050 6850 0    60   ~ 0
SWITCH3e
Text Label 3450 6450 2    60   ~ 0
SWITCH1e
Text Label 3450 6850 2    60   ~ 0
SWITCH3b
Text Label 3450 6950 2    60   ~ 0
SWITCH3a
Text Label 1850 1650 0    60   ~ 0
SWITCH1e
Text Label 1850 1750 0    60   ~ 0
D4
Text Label 1850 1300 0    60   ~ 0
SWITCH1a
Text Label 2650 1300 0    60   ~ 0
SWITCH1b
Text Label 5300 1600 2    60   ~ 0
CLAMP1out
$Comp
L R R12
U 1 1 539CC907
P 3650 2300
F 0 "R12" V 3730 2300 50  0000 C CNN
F 1 "470K" V 3650 2300 50  0000 C CNN
F 2 "Resistors_SMD:R_1206" H 3650 2300 60  0001 C CNN
F 3 "" H 3650 2300 60  0001 C CNN
	1    3650 2300
	0    -1   -1   0   
$EndComp
$Comp
L OPAMP U9
U 1 1 539CC90D
P 3700 2850
F 0 "U9" H 3850 3150 70  0000 C CNN
F 1 "OPAMP2" H 3850 3050 70  0000 C CNN
F 2 "" H 3700 2850 60  0001 C CNN
F 3 "" H 3700 2850 60  0000 C CNN
	1    3700 2850
	1    0    0    1   
$EndComp
$Comp
L SWITCH U7
U 1 1 539CC913
P 2650 2750
F 0 "U7" H 2700 2550 30  0000 L CNN
F 1 "SWITCH2" H 2650 2600 30  0000 L CNN
F 2 "" H 2650 2750 60  0001 C CNN
F 3 "" H 2650 2750 60  0000 C CNN
	1    2650 2750
	1    0    0    -1  
$EndComp
$Comp
L R R9
U 1 1 539CC919
P 2750 2300
F 0 "R9" V 2830 2300 50  0000 C CNN
F 1 "680K" V 2750 2300 50  0000 C CNN
F 2 "Resistors_SMD:R_1206" H 2750 2300 60  0001 C CNN
F 3 "" H 2750 2300 60  0001 C CNN
	1    2750 2300
	0    -1   -1   0   
$EndComp
$Comp
L R R6
U 1 1 539CC91F
P 1950 2300
F 0 "R6" V 2030 2300 50  0000 C CNN
F 1 "100K" V 1950 2300 50  0000 C CNN
F 2 "Resistors_SMD:R_1206" H 1950 2300 60  0001 C CNN
F 3 "" H 1950 2300 60  0001 C CNN
	1    1950 2300
	0    -1   -1   0   
$EndComp
$Comp
L CAPAPOL C2
U 1 1 539CC930
P 1350 2300
F 0 "C2" V 1400 2400 40  0000 L CNN
F 1 "10uF" V 1300 2400 40  0000 L CNN
F 2 "SMD_Packages:SMD-1206_Pol" H 1450 2150 30  0000 C CNN
F 3 "~" H 1350 2300 300 0000 C CNN
	1    1350 2300
	0    1    1    0   
$EndComp
Text Label 4200 2850 0    60   ~ 0
OPAMP2out
Text Label 3550 3200 2    60   ~ 0
OPAMP2in+
Text Label 2700 3200 0    60   ~ 0
Vmid
Text Label 3700 2550 2    60   ~ 0
OPAMP2in-
Text Label 750  2300 0    60   ~ 0
CLAMP2in
Text Label 1850 2900 0    60   ~ 0
SWITCH2e
Text Label 1850 3000 0    60   ~ 0
D7
Text Label 1850 2550 0    60   ~ 0
SWITCH2a
Text Label 2650 2550 0    60   ~ 0
SWITCH2b
Text Label 5300 2850 2    60   ~ 0
CLAMP2out
$Comp
L R R13
U 1 1 539CC952
P 3650 3550
F 0 "R13" V 3730 3550 50  0000 C CNN
F 1 "470K" V 3650 3550 50  0000 C CNN
F 2 "Resistors_SMD:R_1206" H 3650 3550 60  0001 C CNN
F 3 "" H 3650 3550 60  0001 C CNN
	1    3650 3550
	0    -1   -1   0   
$EndComp
$Comp
L OPAMP U10
U 1 1 539CC958
P 3700 4100
F 0 "U10" H 3850 4400 70  0000 C CNN
F 1 "OPAMP3" H 3850 4300 70  0000 C CNN
F 2 "" H 3700 4100 60  0001 C CNN
F 3 "" H 3700 4100 60  0000 C CNN
	1    3700 4100
	1    0    0    1   
$EndComp
$Comp
L SWITCH U15
U 2 1 539CC95E
P 2650 4000
F 0 "U15" H 2700 3800 30  0000 L CNN
F 1 "SWITCH3" H 2650 3850 30  0000 L CNN
F 2 "" H 2650 4000 60  0001 C CNN
F 3 "" H 2650 4000 60  0000 C CNN
	2    2650 4000
	1    0    0    -1  
$EndComp
$Comp
L R R10
U 1 1 539CC964
P 2750 3550
F 0 "R10" V 2830 3550 50  0000 C CNN
F 1 "680K" V 2750 3550 50  0000 C CNN
F 2 "Resistors_SMD:R_1206" H 2750 3550 60  0001 C CNN
F 3 "" H 2750 3550 60  0001 C CNN
	1    2750 3550
	0    -1   -1   0   
$EndComp
$Comp
L R R7
U 1 1 539CC96A
P 1950 3550
F 0 "R7" V 2030 3550 50  0000 C CNN
F 1 "100K" V 1950 3550 50  0000 C CNN
F 2 "Resistors_SMD:R_1206" H 1950 3550 60  0001 C CNN
F 3 "" H 1950 3550 60  0001 C CNN
	1    1950 3550
	0    -1   -1   0   
$EndComp
$Comp
L CAPAPOL C3
U 1 1 539CC97B
P 1350 3550
F 0 "C3" V 1400 3650 40  0000 L CNN
F 1 "10uF" V 1300 3650 40  0000 L CNN
F 2 "SMD_Packages:SMD-1206_Pol" H 1450 3400 30  0000 C CNN
F 3 "~" H 1350 3550 300 0000 C CNN
	1    1350 3550
	0    1    1    0   
$EndComp
Text Label 4200 4100 0    60   ~ 0
OPAMP3out
Text Label 3550 4450 2    60   ~ 0
OPAMP3in+
Text Label 2700 4450 0    60   ~ 0
Vmid
Text Label 3700 3800 2    60   ~ 0
OPAMP3in-
Text Label 750  3550 0    60   ~ 0
CLAMP3in
Text Label 1850 4150 0    60   ~ 0
SWITCH3e
Text Label 1850 4250 0    60   ~ 0
D8
Text Label 1850 3800 0    60   ~ 0
SWITCH3a
Text Label 2650 3800 0    60   ~ 0
SWITCH3b
Text Label 5300 4100 2    60   ~ 0
CLAMP3out
NoConn ~ 2850 6550
NoConn ~ 2850 6650
NoConn ~ 2850 6750
Text Label 3950 5250 2    60   ~ 0
OPAMP4in+
Text Label 3950 5150 2    60   ~ 0
OPAMP4in-
Text Label 3950 5050 2    60   ~ 0
OPAMP4out
$Comp
L OPAMP U11
U 1 1 539CCF9D
P 9500 1900
F 0 "U11" H 9650 2200 70  0000 C CNN
F 1 "OPAMP4" H 9650 2100 70  0000 C CNN
F 2 "" H 9500 1900 60  0001 C CNN
F 3 "" H 9500 1900 60  0000 C CNN
	1    9500 1900
	1    0    0    -1  
$EndComp
$Comp
L C C5
U 1 1 539CD49F
P 8400 2100
F 0 "C5" H 8300 2200 40  0000 L CNN
F 1 "1uF" H 8450 1950 40  0000 L TNN
F 2 "Capacitors_SMD:C_1206" H 8438 1950 30  0000 C CNN
F 3 "~" H 8400 2100 60  0000 C CNN
	1    8400 2100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR012
U 1 1 539CD512
P 8400 2500
F 0 "#PWR012" H 8400 2500 30  0001 C CNN
F 1 "GND" H 8400 2430 30  0001 C CNN
F 2 "" H 8400 2500 60  0000 C CNN
F 3 "" H 8400 2500 60  0000 C CNN
	1    8400 2500
	1    0    0    -1  
$EndComp
Text Label 10500 1900 2    60   ~ 0
Vmid
Text Label 6650 4400 0    60   ~ 0
D9
$Comp
L REF3133 U5
U 1 1 539CC413
P 6400 1700
F 0 "U5" H 6550 1504 60  0000 C CNN
F 1 "REF3133" H 6400 1900 60  0000 C CNN
F 2 "Housings_SOT-23_SOT-143_TSOT-6:SOT-23" H 6400 1700 60  0001 C CNN
F 3 "" H 6400 1700 60  0000 C CNN
	1    6400 1700
	1    0    0    -1  
$EndComp
$Comp
L CONN_2 P5
U 1 1 54A3F3ED
P 4500 4900
F 0 "P5" V 4450 4900 40  0000 C CNN
F 1 "RST" V 4550 4900 40  0000 C CNN
F 2 "SIL-2" H 4500 4900 60  0001 C CNN
F 3 "" H 4500 4900 60  0001 C CNN
	1    4500 4900
	-1   0    0    1   
$EndComp
$Comp
L +5V #PWR013
U 1 1 54A3F42C
P 5150 4800
F 0 "#PWR013" H 5150 4890 20  0001 C CNN
F 1 "+5V" H 5150 4890 30  0000 C CNN
F 2 "" H 5150 4800 60  0001 C CNN
F 3 "" H 5150 4800 60  0001 C CNN
	1    5150 4800
	1    0    0    -1  
$EndComp
Text Label 5200 5000 2    60   ~ 0
Rst
Text Label 6550 3200 0    60   ~ 0
Rst
Text Label 10100 3200 0    60   ~ 0
Rst
$Comp
L C C6
U 1 1 54A41066
P 1000 5650
F 0 "C6" H 1000 5750 40  0000 L CNN
F 1 "100nF" H 1006 5565 40  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 1038 5500 30  0000 C CNN
F 3 "~" H 1000 5650 60  0000 C CNN
	1    1000 5650
	-1   0    0    1   
$EndComp
$Comp
L C C7
U 1 1 54A41226
P 3800 6700
F 0 "C7" H 3800 6800 40  0000 L CNN
F 1 "100nF" H 3806 6615 40  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 3838 6550 30  0000 C CNN
F 3 "~" H 3800 6700 60  0000 C CNN
	1    3800 6700
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR014
U 1 1 54A41447
P 1000 6000
F 0 "#PWR014" H 1000 6000 30  0001 C CNN
F 1 "GND" H 1000 5930 30  0001 C CNN
F 2 "" H 1000 6000 60  0001 C CNN
F 3 "" H 1000 6000 60  0001 C CNN
	1    1000 6000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR015
U 1 1 54A41531
P 3800 7050
F 0 "#PWR015" H 3800 7050 30  0001 C CNN
F 1 "GND" H 3800 6980 30  0001 C CNN
F 2 "" H 3800 7050 60  0001 C CNN
F 3 "" H 3800 7050 60  0001 C CNN
	1    3800 7050
	1    0    0    -1  
$EndComp
Text Label 10750 1750 2    60   ~ 0
OPAMP4out
Text Label 9000 1800 2    60   ~ 0
OPAMP4in+
Text Label 9000 2000 2    60   ~ 0
OPAMP4in-
$Comp
L +5V #PWR016
U 1 1 54A44098
P 10350 3150
F 0 "#PWR016" H 10350 3240 20  0001 C CNN
F 1 "+5V" H 10350 3240 30  0000 C CNN
F 2 "" H 10350 3150 60  0000 C CNN
F 3 "" H 10350 3150 60  0000 C CNN
	1    10350 3150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR017
U 1 1 54A75D12
P 6600 3750
F 0 "#PWR017" H 6600 3750 30  0001 C CNN
F 1 "GND" H 6600 3680 30  0001 C CNN
F 2 "" H 6600 3750 60  0001 C CNN
F 3 "" H 6600 3750 60  0001 C CNN
	1    6600 3750
	1    0    0    -1  
$EndComp
Text Label 7400 5600 0    60   ~ 0
D1
Text Label 7400 5800 0    60   ~ 0
D2
Text Label 7400 5900 0    60   ~ 0
A7
Text Label 7400 5700 0    60   ~ 0
D0
$Comp
L GND #PWR018
U 1 1 54A769EA
P 7150 5550
F 0 "#PWR018" H 7150 5550 30  0001 C CNN
F 1 "GND" H 7150 5480 30  0001 C CNN
F 2 "" H 7150 5550 60  0001 C CNN
F 3 "" H 7150 5550 60  0001 C CNN
	1    7150 5550
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR019
U 1 1 54A769F7
P 7150 5300
F 0 "#PWR019" H 7150 5390 20  0001 C CNN
F 1 "+5V" H 7150 5390 30  0000 C CNN
F 2 "" H 7150 5300 60  0000 C CNN
F 3 "" H 7150 5300 60  0000 C CNN
	1    7150 5300
	1    0    0    -1  
$EndComp
Text Label 6650 3400 0    60   ~ 0
D0
Text Label 6650 3300 0    60   ~ 0
D1
Text Label 6650 3700 0    60   ~ 0
D2
Text Label 10000 3700 0    60   ~ 0
A7
$Comp
L CONN_7 P6
U 1 1 54D53A9C
P 8000 5700
F 0 "P6" V 7970 5700 60  0000 C CNN
F 1 "IO" V 8070 5700 60  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06" H 8000 5700 60  0001 C CNN
F 3 "" H 8000 5700 60  0000 C CNN
	1    8000 5700
	1    0    0    -1  
$EndComp
Text Label 7400 6000 0    60   ~ 0
Vin
Text Label 9850 3200 0    60   ~ 0
Vin
Text Label 6500 6950 0    60   ~ 0
Vmid
Wire Wire Line
	9900 5800 9650 5800
Wire Wire Line
	8600 6200 8850 6200
Wire Wire Line
	6900 3900 6650 3900
Wire Wire Line
	9700 4400 10000 4400
Wire Wire Line
	6500 5400 6500 5700
Wire Wire Line
	10000 4200 9700 4200
Wire Wire Line
	9900 5050 9900 5150
Wire Wire Line
	9900 5150 10250 5150
Wire Wire Line
	9900 3450 9900 3400
Wire Wire Line
	9900 3400 9700 3400
Wire Wire Line
	10100 3500 9700 3500
Wire Wire Line
	6600 3750 6600 3600
Wire Wire Line
	6600 3600 6900 3600
Wire Wire Line
	6550 3500 6900 3500
Wire Wire Line
	9700 3600 10350 3600
Wire Wire Line
	10100 3500 10100 3200
Wire Wire Line
	10250 5350 9900 5350
Wire Wire Line
	9900 5350 9900 5500
Wire Wire Line
	6650 4300 6900 4300
Wire Wire Line
	9700 4300 10000 4300
Wire Wire Line
	6500 6000 6500 6400
Wire Wire Line
	10000 4100 9700 4100
Wire Wire Line
	9700 4000 10000 4000
Wire Wire Line
	6650 4200 6900 4200
Wire Wire Line
	8600 5800 8850 5800
Wire Wire Line
	9650 6200 9900 6200
Wire Wire Line
	6500 6700 6500 6950
Wire Wire Line
	9700 4500 10000 4500
Wire Wire Line
	5850 1450 5850 2050
Wire Wire Line
	5850 1650 6000 1650
Wire Wire Line
	6400 1950 6400 2150
Wire Wire Line
	6800 1650 7050 1650
Wire Wire Line
	6250 2050 6400 2050
Connection ~ 6400 2050
Wire Wire Line
	5850 2050 5950 2050
Connection ~ 5850 1650
Wire Wire Line
	7850 1600 7850 2000
Wire Wire Line
	7850 1800 9000 1800
Connection ~ 7850 1800
Wire Wire Line
	7850 2300 7850 2550
Wire Wire Line
	7850 1000 7850 1300
Wire Wire Line
	2850 6350 3800 6350
Wire Wire Line
	1100 6950 1550 6950
Wire Wire Line
	1250 5450 1950 5450
Wire Wire Line
	1250 5250 1950 5250
Wire Wire Line
	3250 5450 3950 5450
Wire Wire Line
	1950 5150 1250 5150
Wire Wire Line
	1000 5350 1950 5350
Wire Wire Line
	1950 5050 1250 5050
Wire Wire Line
	1250 5550 1950 5550
Wire Wire Line
	1950 5650 1250 5650
Wire Wire Line
	3950 5550 3250 5550
Wire Wire Line
	3250 5650 3950 5650
Wire Wire Line
	4200 1600 5300 1600
Wire Wire Line
	3800 1050 4800 1050
Wire Wire Line
	2900 1050 3500 1050
Wire Wire Line
	3100 1050 3100 1500
Wire Wire Line
	2950 1500 3200 1500
Connection ~ 3100 1050
Wire Wire Line
	2100 1050 2600 1050
Connection ~ 3100 1500
Wire Wire Line
	2300 1500 2350 1500
Wire Wire Line
	2300 1050 2300 1500
Connection ~ 2300 1050
Wire Wire Line
	1550 1050 1800 1050
Wire Wire Line
	4800 1050 4800 1600
Connection ~ 4800 1600
Wire Wire Line
	3200 1700 3000 1700
Wire Wire Line
	3000 1700 3000 1950
Wire Wire Line
	2700 1950 3550 1950
Connection ~ 3000 1950
Wire Wire Line
	750  1050 1150 1050
Wire Wire Line
	1050 6350 1550 6350
Wire Wire Line
	1550 6450 1050 6450
Wire Wire Line
	1050 6550 1550 6550
Wire Wire Line
	1550 6650 1050 6650
Wire Wire Line
	1050 6750 1550 6750
Wire Wire Line
	1550 6850 1050 6850
Wire Wire Line
	1100 6950 1100 7000
Wire Wire Line
	2850 6450 3450 6450
Wire Wire Line
	2850 6850 3450 6850
Wire Wire Line
	3450 6950 2850 6950
Wire Wire Line
	3250 5350 4100 5350
Wire Wire Line
	4100 5350 4100 5400
Connection ~ 2300 1300
Wire Wire Line
	1850 1300 2300 1300
Wire Wire Line
	2650 1300 3100 1300
Connection ~ 3100 1300
Connection ~ 1850 1650
Wire Wire Line
	3200 1500 3200 1300
Connection ~ 3700 1300
Wire Wire Line
	3200 1300 3700 1300
Wire Wire Line
	2350 1650 1850 1650
Wire Wire Line
	1850 1650 1850 1750
Wire Wire Line
	4200 2850 5300 2850
Wire Wire Line
	3800 2300 4800 2300
Wire Wire Line
	2900 2300 3500 2300
Wire Wire Line
	3100 2300 3100 2750
Wire Wire Line
	2950 2750 3200 2750
Connection ~ 3100 2300
Wire Wire Line
	2100 2300 2600 2300
Connection ~ 3100 2750
Wire Wire Line
	2300 2750 2350 2750
Wire Wire Line
	2300 2300 2300 2750
Connection ~ 2300 2300
Wire Wire Line
	1550 2300 1800 2300
Wire Wire Line
	4800 2300 4800 2850
Connection ~ 4800 2850
Wire Wire Line
	3200 2950 3000 2950
Wire Wire Line
	3000 2950 3000 3200
Wire Wire Line
	2700 3200 3550 3200
Connection ~ 3000 3200
Wire Wire Line
	750  2300 1150 2300
Connection ~ 2300 2550
Wire Wire Line
	1850 2550 2300 2550
Wire Wire Line
	2650 2550 3100 2550
Connection ~ 3100 2550
Connection ~ 1850 2900
Wire Wire Line
	3200 2750 3200 2550
Connection ~ 3700 2550
Wire Wire Line
	3200 2550 3700 2550
Wire Wire Line
	2350 2900 1850 2900
Wire Wire Line
	1850 2900 1850 3000
Wire Wire Line
	4200 4100 5300 4100
Wire Wire Line
	3800 3550 4800 3550
Wire Wire Line
	2900 3550 3500 3550
Wire Wire Line
	3100 3550 3100 4000
Wire Wire Line
	2950 4000 3200 4000
Connection ~ 3100 3550
Wire Wire Line
	2100 3550 2600 3550
Connection ~ 3100 4000
Wire Wire Line
	2300 4000 2350 4000
Wire Wire Line
	2300 3550 2300 4000
Connection ~ 2300 3550
Wire Wire Line
	1550 3550 1800 3550
Wire Wire Line
	4800 3550 4800 4100
Connection ~ 4800 4100
Wire Wire Line
	3200 4200 3000 4200
Wire Wire Line
	3000 4200 3000 4450
Wire Wire Line
	2700 4450 3550 4450
Connection ~ 3000 4450
Wire Wire Line
	750  3550 1150 3550
Connection ~ 2300 3800
Wire Wire Line
	1850 3800 2300 3800
Wire Wire Line
	2650 3800 3100 3800
Connection ~ 3100 3800
Connection ~ 1850 4150
Wire Wire Line
	3200 4000 3200 3800
Connection ~ 3700 3800
Wire Wire Line
	3200 3800 3700 3800
Wire Wire Line
	2350 4150 1850 4150
Wire Wire Line
	1850 4150 1850 4250
Wire Wire Line
	3250 5050 3950 5050
Wire Wire Line
	3250 5150 3950 5150
Wire Wire Line
	3950 5250 3250 5250
Wire Wire Line
	6500 6200 6100 6200
Connection ~ 6500 6200
Wire Wire Line
	10000 1900 10500 1900
Wire Wire Line
	10250 1750 10250 2400
Wire Wire Line
	10250 2400 8750 2400
Wire Wire Line
	8750 2400 8750 2000
Wire Wire Line
	8750 2000 9000 2000
Wire Wire Line
	8400 2250 8400 2500
Wire Wire Line
	8400 1800 8400 1950
Connection ~ 8400 1800
Connection ~ 10250 1900
Wire Wire Line
	9700 3900 10650 3900
Wire Wire Line
	6650 4400 6900 4400
Wire Wire Line
	5500 5950 5500 6850
Wire Wire Line
	5500 6350 4850 6350
Wire Wire Line
	4850 5950 5500 5950
Connection ~ 5500 6350
Wire Wire Line
	4850 6550 5350 6550
Wire Wire Line
	4850 6150 5350 6150
Wire Wire Line
	4850 5750 5350 5750
Connection ~ 5500 6800
Wire Wire Line
	4850 5300 5150 5300
Wire Wire Line
	4850 5500 5150 5500
Wire Wire Line
	4850 6750 5500 6750
Wire Wire Line
	5500 6750 5500 6800
Wire Wire Line
	4850 4800 5150 4800
Wire Wire Line
	4850 5000 5200 5000
Wire Wire Line
	1000 5250 1000 5500
Connection ~ 1000 5350
Wire Wire Line
	1000 5800 1000 6000
Wire Wire Line
	3800 6300 3800 6550
Connection ~ 3800 6350
Wire Wire Line
	3800 6850 3800 7050
Wire Wire Line
	10250 1750 10750 1750
Wire Wire Line
	10350 3600 10350 3150
Wire Wire Line
	7150 5300 7150 5400
Wire Wire Line
	7150 5400 7650 5400
Wire Wire Line
	7150 5550 7150 5500
Wire Wire Line
	7150 5500 7650 5500
Wire Wire Line
	7400 5600 7650 5600
Wire Wire Line
	7400 5700 7650 5700
Wire Wire Line
	7650 5800 7400 5800
Wire Wire Line
	7400 5900 7650 5900
Wire Wire Line
	6550 3500 6550 3200
Wire Wire Line
	6650 3300 6900 3300
Wire Wire Line
	6650 3400 6900 3400
Wire Wire Line
	6650 3700 6900 3700
Wire Wire Line
	9700 3800 10650 3800
Wire Wire Line
	10650 3800 10650 3950
Connection ~ 10650 3900
Wire Wire Line
	9700 3700 10000 3700
Wire Wire Line
	7400 6000 7650 6000
Wire Wire Line
	9850 3200 9850 3300
Wire Wire Line
	9850 3300 9700 3300
$EndSCHEMATC
