PCBNEW-LibModule-V1  jue 21 nov 2013 14:32:35 CET
# encoding utf-8
$INDEX
TRANSFO
$EndINDEX
$MODULE TRANSFO
Po 0 0 0 15 528E0B6E 528E0B68 ~~
Li TRANSFO
Sc 528E0B68
AR /528CF34F
Op 0 0 0
T0 7087 12205 600 600 0 120 N V 21 N "T1"
T1 6693 -1181 600 600 0 120 N V 21 N "TRANSFO"
DS 0 0 12598 0 150 21
DS 12598 0 12598 10630 150 21
DS 12598 10630 0 10630 150 21
DS 0 10630 0 0 150 21
$PAD
Sh "4" C 600 600 0 0 0
Dr 354 0 0
At STD N 00E0FFFF
Ne 3 "/V1"
Po 4331 1378
$EndPAD
$PAD
Sh "3" C 600 600 0 0 0
Dr 354 0 0
At STD N 00E0FFFF
Ne 4 "/V2"
Po 8268 1378
$EndPAD
$PAD
Sh "1" C 600 600 0 0 0
Dr 354 0 0
At STD N 00E0FFFF
Ne 2 "/P2"
Po 2362 9252
$EndPAD
$PAD
Sh "2" C 600 600 0 0 0
Dr 354 0 0
At STD N 00E0FFFF
Ne 1 "/P1"
Po 10236 9252
$EndPAD
$EndMODULE  TRANSFO
$EndLIBRARY
