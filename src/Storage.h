#ifndef __STORAGE_H__
#define __STORAGE_H__

enum {
  STORAGE_CALIBRATION_V = 0,
  STORAGE_CALIBRATION_I_LOW_0,
  STORAGE_CALIBRATION_I_LOW_1,
  STORAGE_CALIBRATION_I_LOW_2,
  STORAGE_CALIBRATION_I_HIGH_0,
  STORAGE_CALIBRATION_I_HIGH_1,
  STORAGE_CALIBRATION_I_HIGH_2,
  STORAGE_POWER_LIMIT_0,
  STORAGE_POWER_LIMIT_1,
  STORAGE_POWER_LIMIT_2,
  STORAGE_CURRENT_SIGN_0,
  STORAGE_CURRENT_SIGN_1,
  STORAGE_CURRENT_SIGN_2,
  STORAGE_LOOP_WAVES
};

class Storage
{
 public:

  void setup();

  double read(unsigned int item);

  void write(unsigned int item, double value);

 private:

  void update(unsigned int address, byte val);

};

extern class Storage Storage;

#endif
