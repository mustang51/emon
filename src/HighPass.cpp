#include <Arduino.h>

#include "HighPass.h"

HighPass::HighPass() :
  acc(0), lastVal(0), rounder(1 << (ORDER - 1)), frozen(true)
{
}

long HighPass::filter(const unsigned int value)
{
  long longValue = value;

  long shiftedV  = acc + ((longValue - lastVal) << ORDER);
  acc            = shiftedV - (shiftedV >> ORDER);
  lastVal        = longValue;

  filtered = (shiftedV + rounder) >> ORDER;

  if (!frozen) {
    if (value < min) {
      min = value;
    } else if (value > max) {
      max = value;
    }

    sum += filtered;
  }

  return filtered;
}

void HighPass::reset()
{
  min = 1023;
  max = 0;
  sum = 0;

  frozen = false;
}

void HighPass::freeze()
{
  frozen = true;
}

void HighPass::print()
{
  Serial.print(lastVal - filtered);
  Serial.print(" = ");
  Serial.print(sum);
  Serial.print(" (");
  Serial.print(min);
  Serial.print(" : ");
  Serial.print(max);
  Serial.print(")");
  Serial.print(" = ");
  Serial.println(max - min);

}

bool HighPass::isLocked(const unsigned int samples)
{
  int offset;
  long threshold = samples * 3;

  offset = lastVal - filtered;

  // too far away from the center?
  if ((offset > 512 + MAX_OFFSET) || (offset < 512 - MAX_OFFSET)) {
    return false;
  }

  // misaligned?
  if (abs(sum) > threshold) {
    return false;
  }

  return true;
}

bool HighPass::underflow()
{
  if ((max - min) < UNDERFLOW_THRESHOLD) {
    return true;
  }

  return false;
}

bool HighPass::overflow()
{
  if (max >= (1023 - OVERFLOW_THRESHOLD)) {
    return true;
  }

  if (min <= (OVERFLOW_THRESHOLD)) {
    return true;
  }

  return false;
}
