#ifndef __BUZZER_H__
#define __BUZZER_H__

class Buzzer
{
 public:
  Buzzer();

  void setup(const unsigned char pin);

  void ring(const unsigned char channel);
  void silence(const unsigned char channel);
  void refresh();

 private:
  static const unsigned int RING_PERIOD_MS = 200;

  unsigned char pin;
  unsigned char ringChannels;
  bool ringing;
  unsigned int next;
};

extern class Buzzer Buzzer;

#endif
