#include <Arduino.h>

#include "Buzzer.h"

class Buzzer Buzzer;

Buzzer::Buzzer() : ringChannels(0), ringing(false)
{
}

void Buzzer::setup(const unsigned char pin)
{
  this->pin = pin;

  pinMode(pin, OUTPUT);
}

void Buzzer::ring(const unsigned char channel)
{
  if (! ringChannels) {
    // start ringing
    digitalWrite(pin, HIGH);
    ringing = true;

    next = (unsigned int) millis() + RING_PERIOD_MS;
  }

  // enable the channel
  ringChannels |= 1 << channel;
}

void Buzzer::silence(const unsigned char channel)
{
  // disable the channel
  ringChannels &= ~(1 << channel);

  // turn off the noise if no ringing channels left
  if (! ringChannels) {
    ringing = false;
    digitalWrite(pin, LOW);
  }
}

void Buzzer::refresh()
{
  unsigned int now;

  if (! ringChannels) {
    return;
  }

  now = millis();

  if (now < next) {
    return;
  }

  // change ringing
  if (ringing) {
    digitalWrite(pin, LOW);
    ringing = false;
  } else {
    digitalWrite(pin, HIGH);
    ringing = true;
  }

  next = now + RING_PERIOD_MS;
}
