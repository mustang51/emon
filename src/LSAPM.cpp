#include <Arduino.h>

#include "FastLED.h"
#include "LSAPM.h" 

// Data pin that led data will be written out over
#define DATA_PIN 6

LSAPM::LSAPM()
{
      FastLED.addLeds<WS2812B, DATA_PIN, RGB>(leds, NUM_LEDS);
}

//TODO void LSAPM::refresh(uint32_t ava,uint32_t gen,uint32_t con,uint8_t usage) 
void LSAPM::refresh(uint8_t usage) 
{
  CRGB myRed=CRGB::Green;
  CRGB myGreen=CRGB::Red;
  CRGB myBlue=CRGB::Blue;

  /* DUMMY TEST
  for(int i = 0; i < NUM_LEDS; i++) { leds[i] = myRed; } FastLED.show(); delay(2000);
  for(int i = 0; i < NUM_LEDS; i++) { leds[i] = myGreen; } FastLED.show(); delay(1000);
  for(int i = 0; i < NUM_LEDS; i++) { leds[i] = myBlue; } FastLED.show(); delay(500);
  for(int i = 0; i < NUM_LEDS; i++) { leds[i] = CRGB::Orange; } FastLED.show(); delay(500);
  */
  //TODO  avoid waste time, i could check cached varialbes, if no changes do nothing
  //Normalize usage 
  //Some truncating for no lost last led.
  if (usage>96) usage=100;
  int u=((usage)*(NUM_LEDS))/100;

  //Choose the right color
  CRGB color=myGreen;
  if (usage>90){
    color=myRed;
  }
  else if (usage>70){
    color=CRGB::Orange;  
  }  

  for(int i = 0; i < NUM_LEDS; i++) {
    if (i<u)
      // Turn our current led on to white, then show the leds
      leds[i] = color;
    else 
      leds[i] = 0x010101; //Greysh...
    FastLED.show();
   }
}
