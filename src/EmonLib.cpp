/*
  Emon.cpp - Library for openenergymonitor
  Created by Trystan Lea, April 27 2010
  GNU GPL
*/

#include <Arduino.h>

#include "EmonLib.h"
#include "SampleBuffer.h"
#include "FastADC.h"
#include "HighPass.h"
#include "Buzzer.h"
#include "Storage.h"
#include "Reader.h"
#include "Led.h"

// checks if the signal has just crossed zero
// watch out for counting a cross two times! (e.g: (-) then (0) then (+))
#define checkCross(sample, last) (((sample >= 0) && (last < 0)) ||	\
				  ((sample <= 0) && (last > 0)))

#define forEachCurrentPin(i) for (i = 0; i < NR_CURRENT_PINS; i++)

// ANALOG pins
const static unsigned char DC_OFFSET_PIN   = 0;
const static unsigned char VOLTAGE_PIN     = 1;
const static unsigned char CURRENT_PIN_0   = 3;
const static unsigned char CURRENT_PIN_1   = 2;
const static unsigned char CURRENT_PIN_2   = 4;

const static unsigned char adcPins[] = {VOLTAGE_PIN, CURRENT_PIN_0,
					CURRENT_PIN_1, CURRENT_PIN_2};

class EmonLib EmonLib;

EmonLib::EmonLib()
{
  int i;

  showDCOffsets = false;
  showChannel   = false;
  showSummary   = true;
  gainForceHigh = false;
  gainForceLow  = false;

  // digital gain Pins
  gainPinI[0] = GAIN_PIN_0;
  gainPinI[1] = GAIN_PIN_1;
  gainPinI[2] = GAIN_PIN_2;

  forEachCurrentPin(i) {
    gain[i]      = LOW;
    PHASECAL[i]	 = 1;

    highGainSwitchCounter[i] = 0;
  }
}

void EmonLib::setup(void)
{
  unsigned int i;
  unsigned long start, length;

  Serial.begin(115200);

  Serial.println("EmonLib");

  // read values from EEPROM

  Storage.setup();

  readStorage();

  // set output pins
  forEachCurrentPin(i) {
    pinMode(gainPinI[i], OUTPUT);
    digitalWrite(gainPinI[i], LOW);
  }

  // give a bit of time before reading
  delay(100);

  // set analog pins as inputs
  pinMode(DC_OFFSET_PIN, INPUT);

  Led.setup(NR_CURRENT_PINS + 1);

  FastADC.setup(adcPins, FastADC::defaultPrescaler);

  Buzzer.setup(BUZZER_PIN);

  dcOffset = FastADC.readPin(DC_OFFSET_PIN); // startup value

  start = micros();

  // wait for a few loops of the ADC, and measure the timing
  for (i = 0; i < CALIBRATION_LOOPS; i++) {
    FastADC.waitForData();
    FastADC.dataIsProcessed();
  }

  length = micros() - start;
  analogReadDelay = length / CALIBRATION_LOOPS / (NR_CURRENT_PINS + 1);
}

void EmonLib::loop()
{
  calcVI(loopWaves);

  showStatus();

  parseCommands();
}

//--------------------------------------------------------------------
// emon_calc procedure
// Calculates realPower,apparentPower,powerFactor,Vrms,Irms,kwh increment
// From a sample window of the mains AC voltage and current.
// The Sample window length is defined by the number of half wavelengths
// or crossings we choose to measure.
//--------------------------------------------------------------------
void EmonLib::calcVI(unsigned int waves)
{
  int i;
  float samplesPerSec;  // counter of sampling loops
  unsigned int numberOfSamplesWave;  // counter of sampling loops
  unsigned int crossCount;   // number of times the signal crosses zero

  int filteredV = 0;     // signal minus the DC offset (centered in zero)
  int lastFilteredV = 0; // previous filtered value

  int filteredI[NR_CURRENT_PINS];     // signal minus DC offset

  long sumIwave[NR_CURRENT_PINS];    // to get max Irms
  long maxI[NR_CURRENT_PINS];

  // sumX hold the accumulated sum of squared values for a signal
  long sumV;                // voltage
  long sumI[NR_CURRENT_PINS];  // current
  double sumP[NR_CURRENT_PINS];  // instantaneous power (V * I)

  //------------------------------------------------------------------
  // 0) Setup initial values
  //------------------------------------------------------------------

  unsigned long start	  = millis();
  unsigned int	crossings = waves * 2;
  unsigned int	timeout   = (waves * 20) + 500;	// half a sec extra timeout
  unsigned long due	  = start + timeout;
  unsigned long measureStart;

  if (waves < 10) {
    waves = 50;
  }

  if (waves > 200) {
    waves = 200;  // overflow...
  }

  if (showChannel) {
    SampleBuffer.reset();
  }

  //------------------------------------------------------------------
  // 1) Main measurement loop
  //------------------------------------------------------------------

  // Reset accumulators for loop
  sumV = 0;

  forEachCurrentPin(i) {
    sumI[i]	    = 0;
    sumP[i]	    = 0;
    maxI[i]	    = 0;
    sumIwave[i]	    = 0;

    if (gainForceHigh) {
      gain[i] = HIGH;
    } else if (gainForceLow) {
      gain[i] = LOW;
    }

    digitalWrite(gainPinI[i], (gain[i] == LOW) ? LOW : HIGH);
  }

  //----------------------------------------------------------------
  // Lock the signal crossing zero
  //----------------------------------------------------------------

  // Count number of times looped.
  while ((millis() < due)) {

    //----------------------------------------------------------------
    // Read in raw voltage and subtract estimated 2.5V DC offset, to center to 0V
    //----------------------------------------------------------------
    FastADC.waitForData();

    filteredV = FastADC.filters[0].filtered;

    forEachCurrentPin(i) {
      filteredI[i] = FastADC.filters[i + 1].filtered;

      // flip sign if needed
      if (1 == iSign[i]) {
	filteredI[i] = -filteredI[i];
      }
    }

    FastADC.dataIsProcessed();

    //------------------------------------------------------------------
    // Wait for the voltage wave to be close to 'zero' to start sampling
    //------------------------------------------------------------------

    if ((lastFilteredV < 0) && (filteredV >= 0)) {
      break;
    }

    lastFilteredV = filteredV;
  }

  //----------------------------------------------------------------
  // Signal locked
  //----------------------------------------------------------------

  measureStart = millis();

  FastADC.reset();

  // Count number of times looped.
  for (numberOfSamples = 0, crossCount = 0, numberOfSamplesWave = 0;
       (crossCount < crossings) && (millis() < due) ;
       ) {

    //----------------------------------------------------------------
    // Debugging values?
    //----------------------------------------------------------------

    if (showChannel) {
      if (showFast) {
	SampleBuffer.push(filteredV);
	SampleBuffer.push(filteredI[showChannelID]);
      } else {
	Serial.print(filteredV);
	forEachCurrentPin(i) {
	  Serial.print(" ");
	  Serial.print(filteredI[i]);
	}
	Serial.println();
      }
    }

    //----------------------------------------------------------------
    // Root-mean-square method voltage and current
    //----------------------------------------------------------------

    long sqV = (long) filteredV * (long) filteredV;
    sumV    += sqV;

    double lastFilteredVD    = (double)lastFilteredV;
    double deltaLastFiltered = (double) (filteredV - lastFilteredV);

    forEachCurrentPin(i) {
      long sqI = (long) filteredI[i] * (long) filteredI[i];
      sumIwave[i] += sqI;

      //----------------------------------------------------------------
      // Phase calibration && Instantaneous power calc
      //----------------------------------------------------------------

      // analog values are read serially. This introduces a phase
      // delay when computing together current and voltage. This filter
      // interpolates the voltage to the point where it was supposed to
      // be when the current measure was taken.

      double phaseShiftedV = (lastFilteredVD + PHASECAL[i] * deltaLastFiltered);

      // Instantaneous Power, V * I
      double instP = phaseShiftedV * (double) filteredI[i];
      sumP[i] += (double) instP;
    }

    //----------------------------------------------------------------
    //  Find the number of times the voltage has crossed the initial
    //  voltage
    //  - every 2 crosses we will have sampled 1 wavelength
    //  - so this method allows us to sample an integer number of half
    //    wavelengths which increases accuracy
    //----------------------------------------------------------------

    if ((numberOfSamplesWave > 0) &&
	checkCross(filteredV, lastFilteredV)) {

      crossCount++;

      if (! (crossCount & 0x1)) { // did we finish a wavelength?
	numberOfSamples += numberOfSamplesWave;

	// we just sampled a wavelength, handle instant power
	forEachCurrentPin(i) {
	  sumI[i] += sumIwave[i];

	  long curSumI;

	  curSumI = sumIwave[i] / numberOfSamplesWave;

	  if (curSumI > maxI[i]) { // we have a new maximum power
	    maxI[i] = curSumI;
	  }

	  sumIwave[i] = 0; // reset accumulator for next wavelength
	}

	numberOfSamplesWave = 0;
      }
    }

    //----------------------------------------------------------------
    // Update values for the next loop
    //----------------------------------------------------------------
    lastFilteredV  = filteredV;

    numberOfSamplesWave++;

    Buzzer.refresh();

    //------------------------------------------------------------------
    // read voltage and current values, already filtered by the FastADC
    //------------------------------------------------------------------

    FastADC.waitForData();

    filteredV = FastADC.filters[0].filtered;

    forEachCurrentPin(i) {
      filteredI[i] = FastADC.filters[i + 1].filtered;

      // flip sign if needed
      if (1 == iSign[i]) {
	filteredI[i] = -filteredI[i];
      }
    }

    FastADC.dataIsProcessed();
  }

  FastADC.freeze(); // stop statistic accumulation

  //------------------------------------------------------------------
  // 2) Post loop calculations
  //------------------------------------------------------------------

  unsigned int elapsedTimeMs = millis() - measureStart;

  /* now is a good time to fuck the filters by wasting time
     reading some pins, as the filters will have the chance
     to settle while we crunch numbers */

  temp     = FastADC.readTemp();
  dcOffset = FastADC.readPin(DC_OFFSET_PIN);

  float elapsedTime = ((float) elapsedTimeMs) / 1000.0; // in sec

  frequency = (float)(crossCount / 2) / elapsedTime;

  samplesPerSec = (float) numberOfSamples / elapsedTime;

  unsigned int loopDelay = ((long) 1000 * (long) 1000) / samplesPerSec;

  float phaseStep = (float) analogReadDelay / (float) loopDelay;
  forEachCurrentPin(i) {
    PHASECAL[i] = (float) 1.0 + ((float) (i + 1)) * phaseStep;
  }

  // dump DC offset information if needed

  if (showDCOffsets) {
    Serial.print("D     : ");
    Serial.print(dcOffset);
    Serial.print("  samples: ");
    Serial.println(numberOfSamples);

    Serial.print("V     : ");
    if (!FastADC.filters[0].isLocked(numberOfSamples)) {
	Serial.print("// ");
    }

    FastADC.filters[0].print();

    forEachCurrentPin(i) {
      Serial.print("I[");
      if (gain[i] == LOW) {
	Serial.print("L,");
      } else {
	Serial.print("H,");
      }
      Serial.print(i);
      Serial.print("]: ");
      if (!FastADC.filters[i + 1].isLocked(numberOfSamples)) {
	Serial.print("// ");
      }
      FastADC.filters[i + 1].print();
    }
  }

  // check if filters are spot on
  for (i = 0; i < NR_CURRENT_PINS + 1; i++) {
    if (FastADC.filters[i].isLocked(numberOfSamples)) {
      Led.on(i);
    } else {
      Led.off(i);
    }
  }

  //Calculation of the root of the mean of the voltage and current squared (rms)
  //Calibration coeficients applied.

  double V_RATIO = VCAL / 1023.0;
  Vrms = V_RATIO * sqrt((double) sumV / (double) numberOfSamples);

  forEachCurrentPin(i) {
    double I_RATIO = ((LOW == gain[i]) ? ICAL_LOW[i] : ICAL_HIGH[i]) / 1023.0;

    Irms[i]    = I_RATIO * sqrt((double) sumI[i] / (double) numberOfSamples);
    IrmsMax[i] = I_RATIO * sqrt((double) maxI[i]);

    // Calculation power values
    realPower[i]     = (V_RATIO * I_RATIO *
			(double) sumP[i] / (double) numberOfSamples);
    apparentPower[i] = Vrms * Irms[i];
    powerFactor[i]   = realPower[i] / apparentPower[i];

    // gain calculations

    if (! gainForceLow && ! gainForceHigh) {
      if (LOW == gain[i]) { // do we need to switch to HIGH GAIN?
	if (FastADC.filters[i + 1].underflow()) {
	  highGainSwitchCounter[i]++;
	} else {
	  highGainSwitchCounter[i] = 0;
	}

	if (highGainSwitchCounter[i] >= GAIN_SWITCH_HIGH_ATTEMPTS) {
	  highGainSwitchCounter[i] = 0;

	  gain[i] = HIGH;
	}
      } else { // do we need to switch to LOW GAIN?

	if (FastADC.filters[i + 1].overflow()) {
	  gain[i] = LOW;
	}
      }
    }

    // check power limits to trigger an alarm if needed
    if ((powerAlarm[i] > 0) && (realPower[i] > powerAlarm[i])) {
      Buzzer.ring(i);
    } else {
      Buzzer.silence(i);
    }
  }

  timeSpent = millis() - start;

}

void EmonLib::showStatus()
{
  int i;

  if (showChannel) {
    if (!showFast) {
      return;
    }

    SampleBuffer.dump(2);

    for (i = 0; i < 150; i++) {
      Serial.println("0 0");
    }

    return;
  }

  if (!showSummary) {
    return;
  }

  Serial.print("BASE ");
  Serial.print(dcOffset);
  Serial.print(' ');
  Serial.print(numberOfSamples);
  Serial.print(' ');
  Serial.print(timeSpent);
  Serial.print(' ');
  Serial.print(temp);
  Serial.println();


  Serial.print("VOLT ");
  if (!FastADC.filters[0].isLocked(numberOfSamples)) {
    Serial.println("offline");
  } else {
    Serial.print(Vrms);
    Serial.print(' ');
    Serial.print(frequency);
    Serial.println();
  }

  for(i = 0; i < NR_CURRENT_PINS; i++) {
    Serial.print("CURR ");
    Serial.print(i + 1);
    Serial.print(' ');

    if (!FastADC.filters[i + 1].isLocked(numberOfSamples)) {
      Serial.println("offline");
      continue;
    }

    Serial.print(realPower[i]);
    Serial.print(' ');
    Serial.print(apparentPower[i]);
    Serial.print(' ');
    Serial.print(Irms[i]);
    Serial.print(' ');
    Serial.print(IrmsMax[i]);
    Serial.print(' ');
    Serial.print(powerFactor[i]);
    Serial.print(' ');
    if (LOW == gain[i]) {
      Serial.print("LOW");
    } else {
      Serial.print("HIGH");
    }
    Serial.println();
  }
}

void EmonLib::parseCommands()
{
  String str;

  if (false == Reader.poll(false)) {
    return;
  }

  Reader.parseAsString(str);

  Serial.print(str);

  if (str == "dump") {
    showChannel   = true;
    showFast      = false;
    showDCOffsets = false;
    showSummary   = false;

  } else if (str == "dumpfast0") {
    showChannel   = true;
    showFast      = true;
    showChannelID = 0;
    showDCOffsets = false;
    showSummary   = false;

  } else if (str == "dumpfast1") {
    showChannel   = true;
    showFast      = true;
    showChannelID = 1;
    showDCOffsets = false;
    showSummary   = false;

  } else if (str == "dumpfast2") {
    showChannel   = true;
    showFast      = true;
    showChannelID = 2;
    showDCOffsets = false;
    showSummary   = false;

  } else if (str == "summary") {
    showChannel  = false;
    showDCOffsets = false;
    showSummary   = true;

  } else if (str == "dclevels") {
    showDCOffsets = true;
    showChannel  = false;
    showSummary   = false;

  } else if (str == "gainhigh") {
    gainForceLow  = false;
    gainForceHigh = true;

  } else if (str == "gainlow") {
    gainForceLow  = true;
    gainForceHigh = false;

  } else if (str == "gainauto") {
    gainForceLow  = false;
    gainForceHigh = false;

  } else if (str == "calibrate") {
    Serial.println();
    calibrateV();
    calibrateI(255);
    return;

  } else if (str == "calibratev") {
    Serial.println();
    calibrateV();
    return;

  } else if (str == "calibrate0") {
    Serial.println();
    calibrateI(0);
    return;

  } else if (str == "calibrate1") {
    Serial.println();
    calibrateI(1);
    return;

  } else if (str == "calibrate2") {
    Serial.println();
    calibrateI(2);
    return;

  } else if (str == "storage") {
    Serial.println();
    showStorage();
    return;

  } else if (str == "limits") {
    Serial.println();
    setupLimits();
    return;

  } else if (str == "sign") {
    Serial.println();
    currentSign();
    return;

  } else if (str == "restart") {
    Serial.println();
    delay(100);
    asm volatile ("  jmp 0"); // bye bye

  } else if (str == "loop") {
    Serial.println();
    setupLoopWaves();
    return;

  } else {
    Serial.println(": unknown");
    return;
  }

  Serial.println(": OK");
}

void EmonLib::calibrateV()
{
  double readV;

  // set standard calibration values
  VCAL = 500;

  Serial.println("Read voltage after pressing Enter");
  while(false == Reader.poll(false));

  // get some measurements
  calcVI(150);

  // show them and ask for the values measured by the user
  Serial.print("Voltage");
  Serial.print(" observed? ");

  while (false == Reader.poll(true));
  Reader.parseAsDouble(readV);

  if (readV > 0) {
    VCAL = VCAL * readV / Vrms;
    Storage.write(STORAGE_CALIBRATION_V, VCAL);
  }

  Serial.println();

  readStorage();
}

void EmonLib::calibrateI(const unsigned char channel)
{
  unsigned int i;
  double readV;

  // set standard calibration values
  forEachCurrentPin(i) {
    if ((channel == i) || channel == 255) {
      ICAL_LOW[i]  = 50;
      ICAL_HIGH[i] = 5;
    }
  }

  gainForceHigh = false;
  gainForceLow  = true;

  calcVI(50); // let the gain settle

  Serial.println("Read current (low gain) after pressing Enter");
  while(false == Reader.poll(false));

  // now read
  calcVI(150);

  // show them and ask for the values measured by the user
  Serial.print("Current");
  Serial.print(" observed? ");

  while (false == Reader.poll(true));
  Reader.parseAsDouble(readV);

  if (readV > 0) {
    forEachCurrentPin(i) {
      if ((channel == i) || channel == 255) {
	ICAL_LOW[i] = ICAL_LOW[i] * readV / Irms[i];
	Storage.write((unsigned int) STORAGE_CALIBRATION_I_LOW_0 + i,  ICAL_LOW[i]);
      }
    }
  }

  Serial.println();

  // force high gain and read currents
  gainForceHigh = true;
  gainForceLow  = false;

  calcVI(50); // let the gain settle

  Serial.println("Read current (high gain) after pressing Enter");
  while(false == Reader.poll(false));

  // now read
  calcVI(150);

  // show them and ask for the values measured by the user
  Serial.print("Current");
  Serial.print(" observed? ");

  while (false == Reader.poll(true));
  Reader.parseAsDouble(readV);

  if (readV > 0 ) {
    forEachCurrentPin(i) {
      if ((channel == i) || channel == 255) {
	ICAL_HIGH[i] = ICAL_HIGH[i] * readV / Irms[i];

	Storage.write(STORAGE_CALIBRATION_I_HIGH_0 + i, ICAL_HIGH[i]);
      }
    }
  }

  Serial.println();

  // return to auto gain
  gainForceHigh = false;
  gainForceLow  = false;

  readStorage();
}

void EmonLib::currentSign()
{
  unsigned int i;

  gainForceLow  = true;
  gainForceHigh = false;

  // set standard calibration values
  VCAL = 500;
  forEachCurrentPin(i) {
    ICAL_LOW[i]  = 50;
    ICAL_HIGH[i] = 5;
    iSign[i] = 0;
  }

  Serial.println("Provide load and press Enter");
  while(false == Reader.poll(false));

  // check sign of current
  calcVI(50); // let the gain settle
  forEachCurrentPin(i) {
    if (powerFactor[i] < 0) {
      iSign[i] = 1;
    } else {
      iSign[i] = 0;
    }
  }

  Storage.write(STORAGE_CURRENT_SIGN_0, iSign[0]);
  Storage.write(STORAGE_CURRENT_SIGN_1, iSign[1]);
  Storage.write(STORAGE_CURRENT_SIGN_2, iSign[2]);

  gainForceLow  = false;
  gainForceHigh = false;

  readStorage();
}

void EmonLib::showStorage()
{
  unsigned int i;

  Serial.print("VCAL: ");
  Serial.println(VCAL);

  forEachCurrentPin(i) {
    Serial.print("ICAL_LOW [");
    Serial.print(i);
    Serial.print("]: ");
    Serial.println(ICAL_LOW[i]);
  }

  forEachCurrentPin(i) {
    Serial.print("ICAL_HIGH[");
    Serial.print(i);
    Serial.print("]: ");
    Serial.println(ICAL_HIGH[i]);
  }

  forEachCurrentPin(i) {
    Serial.print("POWER_LIMIT[");
    Serial.print(i);
    Serial.print("]: ");
    Serial.println(powerAlarm[i]);
  }

  forEachCurrentPin(i) {
    Serial.print("CURRENT_SIGN[");
    Serial.print(i);
    Serial.print("]: ");
    Serial.println(iSign[i]);
  }

  Serial.print("LOOP_WAVES: ");
  Serial.println(loopWaves);
}

void EmonLib::setupLimits()
{
  unsigned int i;
  double readV;

  forEachCurrentPin(i) {
    Serial.print("Limit[");
    Serial.print(i);
    Serial.print("]? ");

    while(false == Reader.poll(true));

    Reader.parseAsDouble(readV);
    powerAlarm[i] = readV;

    Serial.println();
  }

  Storage.write(STORAGE_POWER_LIMIT_0, powerAlarm[0]);
  Storage.write(STORAGE_POWER_LIMIT_1, powerAlarm[1]);
  Storage.write(STORAGE_POWER_LIMIT_2, powerAlarm[2]);
}

void EmonLib::setupLoopWaves()
{
  double readV;

  Serial.print("Loop? ");

  while(false == Reader.poll(true));

  Reader.parseAsDouble(readV);
  loopWaves = readV;

  Serial.println();

  Storage.write(STORAGE_LOOP_WAVES, loopWaves);
}

void EmonLib::readStorage()
{
  VCAL          = Storage.read(STORAGE_CALIBRATION_V);
  ICAL_LOW[0]   = Storage.read(STORAGE_CALIBRATION_I_LOW_0);
  ICAL_LOW[1]   = Storage.read(STORAGE_CALIBRATION_I_LOW_1);
  ICAL_LOW[2]   = Storage.read(STORAGE_CALIBRATION_I_LOW_2);
  ICAL_HIGH[0]  = Storage.read(STORAGE_CALIBRATION_I_HIGH_0);
  ICAL_HIGH[1]  = Storage.read(STORAGE_CALIBRATION_I_HIGH_1);
  ICAL_HIGH[2]  = Storage.read(STORAGE_CALIBRATION_I_HIGH_2);
  powerAlarm[0] = Storage.read(STORAGE_POWER_LIMIT_0);
  powerAlarm[1] = Storage.read(STORAGE_POWER_LIMIT_1);
  powerAlarm[2] = Storage.read(STORAGE_POWER_LIMIT_2);
  iSign[0]	= Storage.read(STORAGE_CURRENT_SIGN_0);
  iSign[1]	= Storage.read(STORAGE_CURRENT_SIGN_1);
  iSign[2]	= Storage.read(STORAGE_CURRENT_SIGN_2);
  loopWaves     = Storage.read(STORAGE_LOOP_WAVES);
}
