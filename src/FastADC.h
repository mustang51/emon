#ifndef __FASTADC_H__
#define __FASTADC_H__

#include "HighPass.h"

#define MAX_CHANNELS 4

class FastADC
{
 public:
  FastADC();

  void setup(const unsigned char* pins, const unsigned int prescaler);

  void waitForData(); // blocks until new data is ready

  void dataIsProcessed(); // should be called after data has been used

  void reset(); // start statistics collection

  void freeze(); // stop statistics collection

  int readPin(const unsigned char pin);

  long readTemp(); // against AREF 3.3Volt

  static HighPass filters[MAX_CHANNELS];

  static const unsigned char defaultPrescaler = 64;

 private:

  void setPrescaler(const unsigned char prescaler);

  void waitForRequest();

  void requestIsProcessed();
};

extern class FastADC FastADC;

#endif
