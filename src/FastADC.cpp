#include <Arduino.h>

#include "FastADC.h"
#include "HighPass.h"

#define LOOP_LENGTH_MSECS(x) (x * MSECS_IN_A_SEC)
#define LOOP_LENGTH_USECS(x) (x * USECS_IN_A_SEC)
#define MSECS_IN_A_SEC (1000UL)
#define USECS_IN_A_SEC (1000UL * MSECS_IN_A_SEC)

#define MAX_TIME_DIFF  (1UL << 31UL)

// clear ADLAR in ADMUX (0x7C) to right-adjust the result
// ADCL will contain lower 8 bits, ADCH upper 2 (in last two bits)
// Set REFS1..0 in ADMUX (0x7C) to change reference voltage to the
// proper source (01 is Vcc, 00 is AREF)
#define ADMUX_FROM_SOURCE(s) (B00000000 | s)
#define SOURCE_FROM_ADMUX(a) (B00001111 & a)

#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

static volatile unsigned char ptr;
static volatile unsigned long admux[MAX_CHANNELS];
static volatile bool dataReady;
static volatile bool requestPending;

// HIGH PASS FILTERS
HighPass FastADC::filters[MAX_CHANNELS];

class FastADC FastADC;

FastADC::FastADC()
{
  ptr            = 0;
  ADMUX          = admux[ptr];
  dataReady      = false;
  requestPending = false;
}

void FastADC::setup(const unsigned char* pins, const unsigned int prescaler)
{
  unsigned char channel;

  // set the channel to measure
  for (channel = 0; channel < MAX_CHANNELS; channel++) {
    admux[channel] = ADMUX_FROM_SOURCE(pins[channel]);
    pinMode(A0 + pins[channel], INPUT);
  }

  // Set ADEN in ADCSRA (0x7A) to enable the ADC.
  // Note, this instruction takes 12 ADC clocks to execute
  sbi(ADCSRA, ADEN);

  // clear ADATE in ADCSRA (0x7A) to disable auto-triggering.
  cbi(ADCSRA, ADATE);

  // Clear ADTS2..0 in ADCSRB (0x7B) to set trigger mode to free running.
  // This means that as soon as an ADC has finished, the next will be
  // immediately started. No effect, since ADATE is cleared.
  cbi(ADCSRB, ADTS0);
  cbi(ADCSRB, ADTS1);
  cbi(ADCSRB, ADTS2);

  // Set ADIE in ADCSRA (0x7A) to enable the ADC interrupt.
  // Without this, the internal interrupt will not trigger.
  sbi(ADCSRA, ADIE);

  setPrescaler(prescaler);

  ptr            = 0;
  ADMUX          = admux[ptr];
  dataReady      = false;
  requestPending = false;

  // Set ADSC in ADCSRA (0x7A) to start the first ADC conversion
  sbi(ADCSRA, ADSC);
}

void FastADC::waitForData()
{
  while (false == dataReady); // wait for new data to arrive from the interrupt handler

  // ok, new data is ready, block interrupts and return
  noInterrupts();
}

void FastADC::dataIsProcessed()
{
  dataReady = false; // data is old, we will need a new data set

  interrupts();
}

void FastADC::reset()
{
  unsigned char i;

  noInterrupts();

  for (i = 0; i < MAX_CHANNELS; i++) {
    filters[i].reset();
  }

  interrupts();
}

void FastADC::freeze()
{
  unsigned char i;

  noInterrupts();

  for (i = 0; i < MAX_CHANNELS; i++) {
    filters[i].freeze();
  }

  interrupts();
}

int FastADC::readPin(const unsigned char pin)
{
  int result;

  waitForRequest();

  // clear ADIE in ADCSRA (0x7A) to disable the ADC interrupt.
  cbi(ADCSRA, ADIE);
  ADMUX = ADMUX_FROM_SOURCE(pin);

  // Set ADSC in ADCSRA (0x7A) to start the ADC conversion
  sbi(ADCSRA, ADSC);

  while (bit_is_set(ADCSRA, ADSC));
  result = ADCL | (ADCH << 8);

  // reenable interrupts
  sbi(ADCSRA, ADIE);

  requestIsProcessed();

  return result;
}

long FastADC::readTemp()
{
  long result;

  waitForRequest();

  // clear ADIE in ADCSRA (0x7A) to disable the ADC interrupt.
  cbi(ADCSRA, ADIE);
  ADMUX = _BV(MUX3); // select temperature sensor

  delay(1);
  // Set ADSC in ADCSRA (0x7A) to start the ADC conversion
  sbi(ADCSRA, ADSC);

  while (bit_is_set(ADCSRA, ADSC));
  result = ADCL | (ADCH << 8);

  //1100mV*1024 ADC steps http://openenergymonitor.org/emon/node/1186
  result = ((3 * result) - 324) / 1.22;

  // reenable interrupts
  sbi(ADCSRA, ADIE);

  requestIsProcessed();

  return result;
}

void FastADC::waitForRequest()
{
  requestPending = true;

  while(true == requestPending);
}

void FastADC::requestIsProcessed()
{
  ADMUX = admux[ptr];

  // Set ADSC in ADCSRA (0x7A) to start the ADC conversion
  sbi(ADCSRA, ADSC);
}

void FastADC::setPrescaler(const unsigned char value)
{
  cbi(ADCSRA,ADPS2);
  cbi(ADCSRA,ADPS1);
  cbi(ADCSRA,ADPS0);

  switch (value) {
  case 2:
    ADCSRA |= 1;
    break;
  case 4:
    ADCSRA |= 2;
    break;
  case 8:
    ADCSRA |= 3;
    break;
  case 16:
    ADCSRA |= 4;
    break;
  case 32:
    ADCSRA |= 5;
    break;
  case 64:
    ADCSRA |= 6;
    break;
  case 128:
    ADCSRA |= 7;
    break;
  }
}

// Interrupt service routine for the ADC completion
ISR(ADC_vect)
{
  FastADC::filters[ptr].filter(ADCL | (ADCH << 8));

  ptr++;

  if (ptr >= MAX_CHANNELS) { // last one, back to first channel
    ptr = 0;
    dataReady = true;
  }

  // select next channel in ADMUX
  ADMUX = admux[ptr];

  // Set ADSC in ADCSRA (0x7A) to start the ADC conversion
  if ((ptr == 0) && (true == requestPending)) {
    requestPending = false; // bail out without rearming
    return;
  }

  sbi(ADCSRA, ADSC);
}
