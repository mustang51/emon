#include <Arduino.h>

#include "Reader.h"

class Reader Reader;

void Reader::parseAsString(String& str)
{
  str = String(buffer);

  flush();
}

void Reader::parseAsDouble(double& value)
{
  value = atof(buffer);

  flush();
}

bool Reader::poll( bool echo)
{
  // timed out?
  if (timedOut()) {
    flush();
  }

  // any new data to read?
  while (Serial.available()) {

    if (bufferCnt >= BUFFERSIZE) { // overflow?
      flush();
    }

    if (Serial.peek() == '\r') { // delimiter found?
      buffer[bufferCnt] = '\0';
      Serial.read(); // discard delimiter
      return true; // string is ready
    }

    // push the new byte and keep reading
    buffer[bufferCnt] = Serial.read();

    if (echo) {
      Serial.print(buffer[bufferCnt]);
    }

    bufferCnt++;

    due = millis() + TIMEOUT;
  }

  return false; // no delimiter read, so no string ready
}

bool Reader::timedOut()
{
  unsigned long now = millis();

  if (bufferCnt == 0) {
    return false;
  }

  if ((now > due) || (due - now) > TIMEOUT) {
    return true;
  }

  return false;
}

void Reader::flush()
{
  bufferCnt = 0;
}
