#ifndef __SAMPLE_BUFFER_H__
#define __SAMPLE_BUFFER_H__

struct SampleBufferPack {
  int f1: 10;
  int f2: 10;
  int f3: 10;
  int f4: 10;
  int f5: 10;
  int f6: 10;
  int f7: 10;
  int f8: 10;
} __attribute__((packed));

class SampleBuffer
{
 public:
  SampleBuffer();

  unsigned int cnt();

  unsigned char push(const int value);

  unsigned char pop(int &sample);

  unsigned char get(const unsigned int index, int& value);

  void dump(const unsigned char values_per_line);

  void reset(void);

 private:
  static const unsigned int ELEMENT_MAX = 50;

  struct SampleBufferPack elems[ELEMENT_MAX];

  unsigned int  element_cnt;
  unsigned char element_subcnt;
};

extern class SampleBuffer SampleBuffer;

#endif
