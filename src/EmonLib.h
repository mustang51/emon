/*
  Emon.h - Library for openenergymonitor
  Created by Trystan Lea, April 27 2010
  GNU GPL
*/

#ifndef EmonLib_h
#define EmonLib_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

class EmonLib
{
 public:
  EmonLib();

  void setup(void);

  void loop();

 private:

  void readStorage();

  void showStatus();

  void parseCommands();

  void calibrateV();

  void calibrateI(const unsigned char channel);

  void showStorage();

  void setupLimits();

  void setupLoopWaves();

  void currentSign();

  void calcVI(unsigned int waves);

  static const unsigned char NR_CURRENT_PINS = 3;

  // sets the precision vs speed of the ADC readings
  static const unsigned char ADC_PRESCALER = 64;

  // DIGITAL pins
  static const unsigned char GAIN_PIN_0 = 4;
  static const unsigned char GAIN_PIN_1 = 7;
  static const unsigned char GAIN_PIN_2 = 8;
  static const unsigned char BUZZER_PIN = 9;

  // GAIN constants
  static const unsigned int GAIN_SWITCH_HIGH_ATTEMPTS  = 10;

  static const unsigned int CALIBRATION_LOOPS = 100;

  int temp;            // temperature in the chip
  int dcOffset;

  // OPERATING MODE
  bool showSummary;
  bool showDCOffsets;
  bool showChannel;
  bool showFast;
  unsigned char showChannelID;
  bool gainForceHigh;
  bool gainForceLow;
  unsigned int timeSpent;
  unsigned int loopWaves;

  // CALIBRATIONS
  double VCAL; // voltage calibration;
  double ICAL[NR_CURRENT_PINS]; // current calibration
  double ICAL_LOW[NR_CURRENT_PINS]; // current calibration
  double ICAL_HIGH[NR_CURRENT_PINS]; // current calibration
  double PHASECAL[NR_CURRENT_PINS]; /* phase shift between reading voltage
					and current */

  double iSign[NR_CURRENT_PINS];

  // LIMITS
  unsigned int powerAlarm[NR_CURRENT_PINS];

  // CURRENT GAIN
  unsigned int gain[NR_CURRENT_PINS];
  int gainPinI[NR_CURRENT_PINS];
  unsigned char highGainSwitchCounter[NR_CURRENT_PINS];

  // POWER CALCULATIONS
  unsigned int analogReadDelay;
  float frequency;                       // mains frequency (50 Hz)
  unsigned int numberOfSamples;
  double Vrms;                           // RMS voltage
  double Irms[NR_CURRENT_PINS];          // RMS current
  double IrmsMax[NR_CURRENT_PINS];       // RMS current
  double realPower[NR_CURRENT_PINS];     // instantenous V *  instantaneous I
  double apparentPower[NR_CURRENT_PINS]; // total V * total I
  double powerFactor[NR_CURRENT_PINS];   // real / apparent
};

extern class EmonLib EmonLib;

#endif
