#ifndef LSAPM_h
#define LSAPM_h

#include "Arduino.h"
#include "FastLED.h"

#define NUM_LEDS 19

class LSAPM
{
 public:

  CRGB leds[NUM_LEDS];


  LSAPM();

  void setup(void);

  //TODO void refresh(uint32_t ava,uint32_t gen,uint32_t con,uint8_t usage);
  void refresh(uint8_t usage);

 private:
};
#endif


