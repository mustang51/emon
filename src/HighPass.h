#ifndef __HIGHPASS_H__
#define __HIGHPASS_H__

class HighPass
{
 public:
  HighPass();

  long filter(const unsigned int value);

  void print();

  void reset();  // start statistics collection

  void freeze(); // stop statistics collection

  bool isLocked(const unsigned int samples);

  bool underflow();

  bool overflow();

  long filtered;

  long acc; // memory for the filter

 private:
  static const long ORDER = 8;
  static const long MAX_OFFSET = 100;
  static const unsigned int UNDERFLOW_THRESHOLD = 70;
  static const unsigned int OVERFLOW_THRESHOLD  = 10;

  long lastVal;

  long rounder;

  // statistics
  unsigned int min;
  unsigned int max;
  long sum;

  bool frozen;
};

#endif
