#ifndef __READER_H__
#define __READER_H__

class Reader
{
 public:
  bool poll(bool echo);

  void parseAsString(String& str);
  void parseAsDouble(double& value);

 private:
  static const unsigned int TIMEOUT = 30000; // 30 sec
  static const unsigned int BUFFERSIZE = 40;

  char buffer[BUFFERSIZE];
  unsigned char bufferCnt;
  unsigned long due;

  bool timedOut();
  void flush();
};

extern class Reader Reader;

#endif
