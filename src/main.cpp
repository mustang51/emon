// EmonLibrary examples openenergymonitor.org, Licence GNU GPL V3

#include "EmonLib.h"  // Include Emon Library

void setup()
{
  EmonLib.setup();
}

void loop()
{
  EmonLib.loop();
}
