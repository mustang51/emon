#include <Arduino.h>

#include "Led.h"

#define LED_PIN 13

class Led Led;

Led::Led() : pin(LED_PIN), status(0), allOn(0)
{
}

void Led::setup(const unsigned char nrChannels)
{
  unsigned int i;

  pinMode(pin, OUTPUT);

  digitalWrite(pin, LOW);

  // create the mask of "all channels on"
  for (i = 0; i < nrChannels; i++) {
    allOn |= (1 << i);
  }
}

void Led::on(const unsigned char channel)
{
  if (status & (1 << channel)) {
    return; // already on
  }

  status |= (1 << channel);

  if (status == allOn) {
    digitalWrite(pin, HIGH);
  }
}

void Led::off(const unsigned char channel)
{
  if (!(status & (1 << channel))) {
    return; // already off
  }

  // disable the channel
  status &= ~(1 << channel);

  digitalWrite(pin, LOW);
}

