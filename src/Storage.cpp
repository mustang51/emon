#include <Arduino.h>
#include <EEPROM.h>

#include "Storage.h"

class Storage Storage;

void Storage::setup()
{
}

double Storage::read(unsigned int item)
{
  double ret;
  byte* b;
  unsigned int address;

  address = item * 4;

  b = (byte*) &ret;

  b[0] = EEPROM.read(address + 0);
  b[1] = EEPROM.read(address + 1);
  b[2] = EEPROM.read(address + 2);
  b[3] = EEPROM.read(address + 3);

  return ret;
}

void Storage::write(unsigned int item, double value)
{
  byte* b;
  unsigned int address;

  address = item * 4;

  b = (byte *) &value;

  update(address + 0, b[0]);
  update(address + 1, b[1]);
  update(address + 2, b[2]);
  update(address + 3, b[3]);
}

void Storage::update(unsigned int address, byte val)
{
  // write only if needed
  if (val == EEPROM.read(address)) {
    return;
  }

  EEPROM.write(address, val);
}
