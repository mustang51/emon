#ifndef __LED_H__
#define __LED_H__

class Led
{
 public:
  Led();

  void setup(const unsigned char nrChannels);

  void on(const unsigned char channel);
  void off(const unsigned char channel);

 private:

  unsigned char pin;
  unsigned char status;
  unsigned char allOn;
};

extern class Led Led;

#endif











