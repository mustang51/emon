#include <Arduino.h>

#include "SampleBuffer.h"

class SampleBuffer SampleBuffer;

SampleBuffer::SampleBuffer() :
  element_cnt(0), element_subcnt(0)
{
}

unsigned int SampleBuffer::cnt()
{
  return (element_cnt * 8) + element_subcnt;
}

void SampleBuffer::reset()
{
  element_cnt    = 0;
  element_subcnt = 0;
}

unsigned char SampleBuffer::push(const int value)
{
  if (element_cnt >= ELEMENT_MAX) {
    return 1;
  }

  switch(element_subcnt) {
  case 0:
    elems[element_cnt].f1 = value;
    element_subcnt++;
    break;
  case 1:
    elems[element_cnt].f2 = value;
    element_subcnt++;
    break;
  case 2:
    elems[element_cnt].f3 = value;
    element_subcnt++;
    break;
  case 3:
    elems[element_cnt].f4 = value;
    element_subcnt++;
    break;
  case 4:
    elems[element_cnt].f5 = value;
    element_subcnt++;
    break;
  case 5:
    elems[element_cnt].f6 = value;
    element_subcnt++;
    break;
  case 6:
    elems[element_cnt].f7 = value;
    element_subcnt++;
    break;
  case 7:
    elems[element_cnt].f8 = value;
    element_cnt++;
    element_subcnt = 0;
    break;
  }

  return 0;
}

unsigned char SampleBuffer::pop(int& sample)
{
  if ((element_cnt == 0) && (element_subcnt == 0)) {
    return 1;
  }

  switch(element_subcnt) {
  case 0:
    sample = elems[element_cnt].f1;
    element_subcnt = 7;
    element_cnt--;
    break;
  case 1:
    sample = elems[element_cnt].f2;
    element_subcnt--;
    break;
  case 2:
    sample = elems[element_cnt].f3;
    element_subcnt--;
    break;
  case 3:
    sample = elems[element_cnt].f4;
    element_subcnt--;
    break;
  case 4:
    sample = elems[element_cnt].f5;
    element_subcnt--;
    break;
  case 5:
    sample = elems[element_cnt].f6;
    element_subcnt--;
    break;
  case 6:
    sample = elems[element_cnt].f7;
    element_subcnt--;
    break;
  case 7:
    sample = elems[element_cnt].f8;
    element_subcnt--;
    break;
  }

  return 0;
}

unsigned char SampleBuffer::get(const unsigned int index, int& value)
{
  unsigned int element_index;
  unsigned char element_subindex;

  if (index >= cnt()) {
    return 1;
  }

  element_index    = index >> 3;
  element_subindex = index & 0x07;

  switch(element_subindex) {
  case 0:
    value = elems[element_index].f1;
    break;
  case 1:
    value = elems[element_index].f2;
    break;
  case 2:
    value = elems[element_index].f3;
    break;
  case 3:
    value = elems[element_index].f4;
    break;
  case 4:
    value = elems[element_index].f5;
    break;
  case 5:
    value = elems[element_index].f6;
    break;
  case 6:
    value = elems[element_index].f7;
    break;
  case 7:
    value = elems[element_index].f8;
    break;
  }

  return 0;
}

void SampleBuffer::dump(const unsigned char values_per_line)
{
  int sample;
  unsigned int count;
  unsigned int value_count;

  value_count = 0;
  count       = 0;

  while(0 == get(count, sample)) {
    count++;

    Serial.print(sample);
    value_count++;

    if (value_count == values_per_line) {
      Serial.println("");
      value_count = 0;
    } else {
      Serial.print(" ");
    }
  }
}




